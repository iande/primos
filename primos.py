num = int(input("Dame un número entero no negativo: "))
for n in range(1,num+1):
    m = 2
    primo = True
    while primo and m < n:
        if n % m == 0:
            primo = False
        else:
            m += 1
    if primo:
        print("Números primos iguales o menores: ",n)